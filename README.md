Challenge: radio? (maybe come up with a better name)
Type: Stegonography
Difficulty: Easy/Medium

The only thing that needs to be accessed by the contestants is challenge.mp3. I'd just host it on ctfd.

A solution is available in fft_solver.py. Running it will reverse a QR code from the audio and place it in the solution folder.

I might need to do a writeup that walks through the solution.

The flag is in flag.txt
