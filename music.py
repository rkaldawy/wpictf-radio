from pydub import AudioSegment
from scanner import *

songs = [("futurefunk.mp3", 34),
         ("crazy_on_emotion.mp3", 3),
         ("halo.mp3", 40),
         ("lamb.mp3", 127),
         ("cartalk.mp3", 37 * 60 + 5),
         ("fortunate_son.mp3", 18),
         ("gachi.mp3", 2),
         ("deja_vu.mp3", 54),
         ("bitch_lasagna.mp3", 38),
         ("amy.mp3", 103),
         ("memri.mp3", 59),
         ("acdc.mp3", 60),
         ("tupac.mp3", 13),
         ("running.mp3", 22),
         ("gtarap.mp3", 20),
         ("voodoo.mp3", 70),
         ("hardbass.mp3", 1),
         ("hacker.mp3", 33),
         ("fuckme.mp3", 23),
         ("tougen_renka.mp3", 3),
         ("pink.mp3", 70),
         ("tetrus.mp3", 2),
         ("lethal.mp3", 15),
         ("godonlyknows.mp3", 2),
         ("redzone.mp3", 21),
         ("jazz.mp3", 20),
         ("cheeki.mp3", 2)
         ]

clips_dict = {}
clips = []
for name, start in songs:
    clip = AudioSegment.from_mp3("music/" + name)
    clip = clip[start*1000:] + clip[0:start*1000]
    clips_dict[name] = clip

clips_dict["cartalk.mp3"] += 30
clips_dict["tetrus.mp3"] = clips_dict["tetrus.mp3"][5000:]
clips_dict["gachi.mp3"] = clips_dict["gachi.mp3"][19000:45000]  + clips_dict["gachi.mp3"][59000:84000]
clips_dict["cheeki.mp3"] = clips_dict["cheeki.mp3"][12000:]

clips = [v for k, v in clips_dict.items()]

static = AudioSegment.from_mp3("music/radio_static.mp3")
static_start = ((60 * 2) + 39) * 1000
static += 10

qr = img_to_array("flag.png")
qr = qr.flatten().tolist()

print(qr[25*12 : 25*13])

pattern = []
idx = 0
while idx < len(qr):
    count = 0
    target = qr[idx]
    while idx < len(qr) and qr[idx] == target:
        count += 1
        idx += 1
    pattern += [count]

output = static[static_start:static_start+3000]

color = 0
total_len = 0
music_idx = 0

for elt in pattern:
    if color == 0:
        output += static[static_start:static_start + elt*250]
        total_len += 250 * elt
    else:
        clip = clips[music_idx]
        start = total_len % len(clip)
        end = (total_len + 1000 * elt) % len(clip)
        if start < end:
            output += clips[music_idx][start:end]
        else:
            output += clips[music_idx][start:]
            output += clips[music_idx][0:end]
        total_len += 1000 * elt
        music_idx = (music_idx + 1) % len(clips)

    if color == 0:
        color = 1
    else:
        color = 0

output += static[static_start:static_start+3000]

finale = AudioSegment.from_mp3("music/modjo.mp3")
finale = finale[(60 * 3 + 19)*1000:]
output += finale

output.export("challenge.mp3", format="mp3")
