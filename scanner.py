from PIL import Image
import numpy as np


def img_to_array(file):
    image = Image.open(file)
    im = image.convert("RGB")

    width, height  = im.size

    print(width, height)

    array = np.zeros((25, 25))

    x, y = 0, 0

    for i in range(12, (200-13), 7):
        x = 0
        for j in range(12, (200-13), 7):
            r, g, b = im.getpixel((j, i))
            if r == 255:
                array[y][x] = 1
            x += 1
        y += 1

    return array


